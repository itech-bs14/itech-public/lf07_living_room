import paho.mqtt.client as mqtt
from database import insert_temperature, calculate_average_temperature

# MQTT-Parameter
broker_ip = "10.14.213.223"
topic_data = "cdp/room1/temp/data"
topic_display = "cdp/room1/temp/display"


# Callback-Funktion für empfangene MQTT-Nachrichten
def on_message(client, userdata, msg):
    payload = msg.payload.decode()
    temperature = float(payload)

    # Temperatur in die Datenbank einfügen
    insert_temperature(temperature)

    # Arithmetisches Mittel berechnen
    average_temperature = calculate_average_temperature()

    # Durchschnittstemperatur publizieren
    if average_temperature is not None:
        client.publish(topic_display, str(average_temperature))
        print(f"Published average temperature: {average_temperature}")


def setup_mqtt_client():
    client = mqtt.Client()

    # Setze die neuen Callback-Funktionen
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(broker_ip)
    return client


# Callback-Funktion für erfolgreiche Verbindungen
def on_connect(client, userdata, flags, rc):
    print("Verbunden mit dem Broker")
    client.subscribe(topic_data)
