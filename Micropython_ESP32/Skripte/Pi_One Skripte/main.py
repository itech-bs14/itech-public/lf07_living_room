import time
from database import initialize_database
from mqtt_handler import setup_mqtt_client


def main():
    # Datenbank initialisieren
    initialize_database()

    # MQTT-Client konfigurieren
    client = setup_mqtt_client()

    # Endlosschleife, um Nachrichten zu empfangen und zu verarbeiten
    client.loop_start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print("Programm beendet.")
    finally:
        client.loop_stop()
        client.disconnect()


if __name__ == "__main__":
    main()