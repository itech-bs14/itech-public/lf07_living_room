import sqlite3

db_name = 'temperature_data.db'


def initialize_database():
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute('''
        CREATE TABLE IF NOT EXISTS temperature_data (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
            temperature REAL
        )
    ''')
    conn.commit()
    conn.close()


def insert_temperature(temperature):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute('''
        INSERT INTO temperature_data (temperature)
        VALUES (?)
    ''', (temperature,))
    conn.commit()
    conn.close()


def calculate_average_temperature():
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute('''
        SELECT temperature FROM temperature_data
        ORDER BY timestamp DESC
        LIMIT 10
    ''')
    rows = c.fetchall()
    conn.close()

    if rows:
        temperatures = [row[0] for row in rows]
        average_temperature = sum(temperatures) / len(temperatures)
    else:
        average_temperature = None

    return average_temperature