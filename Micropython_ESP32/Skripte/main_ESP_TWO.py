import network
from umqtt.robust import MQTTClient
from machine import Pin
import time
import dht

# Der esp32 empfängt temp daten via mqtt und reagiert bei überschreitung einer Schwelle


LED = Pin(2,Pin.OUT)
# Setze die Werte entsprechend deiner WLAN Konfiguration

#ITECH
ssid = "SSID"
password = "password"
broker_ip = "IP"

topic = "cdp/room1/temp/cmd"

# Funktion zur Verbindung mit dem WLAN
def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    
    if not wlan.isconnected():
        print(f"Verbindung zum WLAN wird hergestellt... SSID: {ssid}")
        wlan.connect(ssid, password)
        
        while not wlan.isconnected():
            time.sleep(1)
    
    print("Verbunden mit dem WLAN.")
    return wlan

# Funktion zum Empfangen von MQTT-Nachrichten
def on_message(topic, msg):
    actor = float(msg)
    print(f"Empfangene Nachricht im Topic '{topic}': Aktor Status {actor}")

    # Überprüfe, ob die Temperatur größer als Schwellenwert ist
    if actor == 1:
        print("Die Temperatur ist kritisch!")
        # Füge hier den Code zum Ausführen des gewünschten Befehls hinzu
        # z.B., subprocess.run("notify-send 'Die Temperatur ist kritisch!'", shell=True)
        LED.value(1)
    else:
        LED.value(0)
        
# Verbinde mit dem WLAN
wifi = connect_to_wifi()

# Erstelle einen MQTT-Client
client = MQTTClient("esp32", broker_ip)
client.set_callback(on_message)

# Verbinde mit dem MQTT-Broker
client.connect()

# Abonniere das gewünschte Topic
client.subscribe(topic)

# Loop für den Empfang von MQTT-Nachrichten
try:
    while True:
        client.wait_msg()
except KeyboardInterrupt:
    print("Programm beendet.")
finally:
    # Trenne die Verbindung zum MQTT-Broker
    client.disconnect()
    # Trenne die WLAN-Verbindung
    wifi.disconnect()
    wifi.active(False)
