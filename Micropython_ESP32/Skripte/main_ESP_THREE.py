import network
from umqtt.simple import MQTTClient
from machine import Pin
import dht
import time
import neopixel
import select

# Das Skript liest enen Sensor aus und schickt Messdaten (Temperatur) via MQTT unter einem topic (topic1) an den Broker.
# Es abonniert einen weiteren topic (t0pic2), der einen Aktor steuert hier eine Datenvisualisierung für Temperatur.


# Pin-Belegungen für DHT11-Sensor
dht_pin = 14
# Pin Belegung für den Aktor (LED)
LED = Pin(2, Pin.OUT)

# Der esp32 sendet und empfängt DHT11-Daten via mqtt
# Setze die Werte entsprechend deiner WLAN Konfiguration

#ITECH
ssid = "SSID"
password = "password"
broker_ip = "IP"

#GM Lab
#ssid = "FRITZ!Box 7520 LN"
#password = "69687413979052172302"
#broker_ip = "192.168.178.30"

# topic der gesendeten msg (Tempdaten)
topic1 = "cdp/room1/temp/data"
# topic der empfangenen msg (Bei 1 Aktor aktiv Bei 0 Aktor passiv)
topic2 = "cdp/room1/temp/display"

# Funktion zur Verbindung mit dem WLAN
def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    
    if not wlan.isconnected():
        print(f"Verbindung zum WLAN wird hergestellt... SSID: {ssid}")
        wlan.connect(ssid, password)
        
        while not wlan.isconnected():
            time.sleep(1)
    
    print("Verbunden mit dem WLAN.")
    return wlan

# Funktion zum Auslesen von DHT11-Daten
def read_dht():
    sensor_data = dht.DHT11(Pin(dht_pin))
    sensor_data.measure()
    temp = sensor_data.temperature()
    hum = sensor_data.humidity()
    print(f"Gesendete Daten im Topic '{topic1}': '{temp}' Grad Celsius")
    return temp, hum

# Funktion zum Empfangen von MQTT-Nachrichten im topic2
def on_message_topic2(topic, msg):
    termtemp = float(msg)
    print(f"Empfangene Nachricht im Topic '{topic}': Aktor Status {termtemp}")
    t2 = 0.05

    pixels = neopixel.NeoPixel(Pin(16, Pin.OUT), 10)
    
    pixels = set_led_colors(termtemp)
     
    pixels.write()
    time.sleep(t2)
    
    
# Funktion zur Ansteuerung der LEDs auf dem Thermometer

def set_led_colors(termtemp):
    pixelx = neopixel.NeoPixel(Pin(16, Pin.OUT), 10)
    
    if termtemp <= 16:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 0, LED 1 - Blau
    elif 16 < termtemp <= 18:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 0, LED 1 - Blau
        pixelx[2] = (0, 0, 255)  # LED 0, LED 1 - Blau
    elif 18.1 <= termtemp <= 20:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 0, LED 1 - Blau
        pixelx[2] = (0, 0, 255)  # LED 0, LED 1 - Blau
        pixelx[3] = (0, 0, 255)  # LED 0, LED 1 - Blau
    elif 20.1 <= termtemp <= 21.5:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 1, - Blau
        pixelx[2] = (0, 0, 255)  # LED 2, - Blau
        pixelx[3] = (0, 0, 255)  # LED 3, - Blau
        pixelx[4] = (0, 255, 0)  # LED 4, - Grün
    elif 21.6 <= termtemp <= 22:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 1, - Blau
        pixelx[2] = (0, 0, 255)  # LED 2, - Blau
        pixelx[3] = (0, 0, 255)  # LED 3, - Blau
        pixelx[4] = (0, 255, 0)  # LED 4, - Grün
        pixelx[5] = (0, 255, 0)  # LED 5, - Grün
    elif 22.1 <= termtemp <= 24:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 1, - Blau
        pixelx[2] = (0, 0, 255)  # LED 2, - Blau
        pixelx[3] = (0, 0, 255)  # LED 3, - Blau
        pixelx[4] = (0, 255, 0)  # LED 4, - Grün
        pixelx[5] = (0, 255, 0)  # LED 5, - Grün
        pixelx[6] = (0, 255, 0)  # LED 6, - Grün
    elif 24.1 <= termtemp <= 25.5:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 1, - Blau
        pixelx[2] = (0, 0, 255)  # LED 2, - Blau
        pixelx[3] = (0, 0, 255)  # LED 3, - Blau
        pixelx[4] = (0, 255, 0)  # LED 4, - Grün
        pixelx[5] = (0, 255, 0)  # LED 5, - Grün
        pixelx[6] = (0, 255, 0)  # LED 6, - Grün
        pixelx[7] = (255, 0, 0)  # LED 7, - Rot
    elif 25.6 <= termtemp <= 28:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 1, - Blau
        pixelx[2] = (0, 0, 255)  # LED 2, - Blau
        pixelx[3] = (0, 0, 255)  # LED 3, - Blau
        pixelx[4] = (0, 255, 0)  # LED 4, - Grün
        pixelx[5] = (0, 255, 0)  # LED 5, - Grün
        pixelx[6] = (0, 255, 0)  # LED 6, - Grün
        pixelx[7] = (255, 0, 0)  # LED 7, - Rot
        pixelx[8] = (255, 0, 0)  # LED 8, - Rot
    elif termtemp > 28:
        pixelx[0] = (255, 255, 255)  # LED 0 - Weiß
        pixelx[1] = (0, 0, 255)  # LED 1, - Blau
        pixelx[2] = (0, 0, 255)  # LED 2, - Blau
        pixelx[3] = (0, 0, 255)  # LED 3, - Blau
        pixelx[4] = (0, 255, 0)  # LED 4, - Grün
        pixelx[5] = (0, 255, 0)  # LED 5, - Grün
        pixelx[6] = (0, 255, 0)  # LED 6, - Grün
        pixelx[7] = (255, 0, 0)  # LED 7, - Rot
        pixelx[8] = (255, 0, 0)  # LED 8, - Rot
        pixelx[9] = (255, 0, 0)  # LED 9, - Rot
    
    return pixelx


# Verbinde mit dem WLAN
wifi = connect_to_wifi()
# Erstelle einen MQTT-Client
client = MQTTClient("esp_three", broker_ip) # jedes Skript in demselben System muss hier einen eigenen Namen für den client haben
client.set_callback(on_message_topic2)  # Setze den Callback für topic2

# Verbinde mit dem MQTT-Broker
client.connect()

# Abonniere das gewünschte Topic (topic2)
client.subscribe(topic2)

# Loop für das Senden von DHT11-Daten über MQTT (topic1) und Empfangen von Nachrichten (topic2)

try:
    while True:
        # Read DHT11 data
        temperature, humidity = read_dht()

        # Publish temperature data via MQTT
        payload_topic1 = str(float(temperature))
        client.publish(topic1, payload_topic1)

        # Check for incoming MQTT messages (non-blocking)
        client.check_msg()

        # Wait for a short time between readings
        time.sleep(1)

except KeyboardInterrupt:
    print("Programm beendet.")
finally:
    # Disconnect from the MQTT broker
    client.disconnect()
    # Disconnect from the WLAN
    wifi.disconnect()
    wifi.active(False)








