# ESP_ONE - Daten erheben und via MQTT versenden

Der Esp32 MC ESP_ONE hat einen Sensor (DHT_11). Dieser kann Luftfeuchtigkeit und Temperatur Messen.

Die Skripte auf dem ESP32 sind in Micropython geschrieben, da die Schüler in den vorherigen LF bereits erste Erfahrungen mit Python gemacht haben und es hier keine Umstellung gibt.

Die gemessene Temperatur wird unter einem Topic via MQTT an den Broker gesendet.

**MAC Adresse ESP_ONE:**

## Vorbereitung

- [ ] Installieren von [Thonny](https://thonny.org/) als IDE und zum flashen von Micropython auf den ESP32
  - [ ] Download [Micropython](https://micropython.org/download/)
  - [ ] Flashen von Micropython auf den ESP32.  
    <https://youtu.be/1efsWgK_QaU?si=gooJbZJtA0LSO-N9>
- [ ] Mac Adresse auslesen um den ESP32 im Schul WLAN anmelden

  

```Python
import network

# Initialize the network interface
wlan = network.WLAN(network.STA_IF)
# Activate the WLAN Interface
wlan.active(True)

# Check if the interface is active (connected)
if wlan.active():
    # Get the MAC address
    mac_address = wlan.config("mac")
    print(mac_address)
    print("Device MAC Address:", ":".join(["{:02X}".format(byte) for byte in mac_address]))
else:
    print("Wi-Fi is not active.")
```

Nach dem flashen von Micropython sind zwei .py Skripte auf dem ESP32 (Boot.py und main.py). Es wird die main.py jeweils überschrieben. Das Skript s.o. als main.py auf dem ESP32 speichern und ausführen. Mac Adresse erscheint im Terminal.

## Das Skript auf dem ESP_ONE in drei Schritten

DHT11 Temperatur und Luftfeuchtigkeitsmessung auf dem Esp32

### Verkabelung:

DHT-Sensoren haben vier Pins, wie in der folgenden Abbildung dargestellt. Wenn Sie Ihren DHT-Sensor jedoch auf einer Breakout-Platine erhalten, hat er nur drei Pins und einen internen Pull-up-Widerstand an Pin 2.

![DHT22](./_GRAFIKEN/dht22-pinout.webp)

Die folgende Tabelle zeigt die Pinbelegung des DHT22 und DHT11. Wenn der Sensor Ihnen zugewandt ist, beginnt die Pin-Nummerierung bei 1 von links nach rechts

|DHT Pin|Verbunden mit|
|--------|------------|
|1|3,3 V|
|2|Beliebiger digitaler GPIO; schließen Sie auch einen **10k Ohm** Pull-up-Widerstand an. In unserem Beispiel ist es Pin 14|
|3|nicht verbinden|
|4|GND|

![](./_GRAFIKEN/dht_esp32_bb-1.webp)

### Step01 - Sensor mit Micropython in der shell / Thonny testen:

```Micropython:
from machine import Pin
import dht

sensor_data = dht.DHT11(Pin(14))

while True: 
    try: 
        sensor_data.measure()
        sensor_data.measure()
        print(sensor_data.temperature())
    except OSError as e:
        print("Lesen des Sensors war nicht erfolgreich.")
```

### Step02 - temp und hum in Endlosschleife auslesen und ausgeben (main.py)

```python:
from machine import Pin
import dht
import time
 
sensor_data = dht.DHT11(Pin(14))
 
def call_dht():
    sensor_data.measure()
    temp = sensor_data.temperature()
    hum = sensor_data.humidity()
    print('Temperatur - ',temp,'Luftfeuchtigkeit',hum)
    

while True:
    try:
        call_dht()
        time.sleep(1)
    except OSError as e:
        print("Lesen des Sensors war nicht erfolgreich.")
```

### Step03 - Ausgelesene Daten via MQTT an Broker senden (main_ESP_ONE.py als main.py ausführen)

```Python:

import network
from umqtt.simple import MQTTClient
from machine import Pin
import dht
import time

# Das Skript liest die Messdaten eines Sensors (DHT11) aus und übermittelt die Temp daten vis MQTT unter einem Tag

# Pin-Belegungen für DHT11-Sensor
dht_pin = 14

# Der esp32 sendet DHT11-Daten via mqtt
# Setze die Werte entsprechend deiner WLAN Konfiguration

#ITECH
ssid = "SSID"
password = "password"
broker_ip = "IP"

topic = "/CDP/testtopic"

# Funktion zur Verbindung mit dem WLAN
def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    
    if not wlan.isconnected():
        print(f"Verbindung zum WLAN wird hergestellt... SSID: {ssid}")
        wlan.connect(ssid, password)
        
        while not wlan.isconnected():
            time.sleep(1)
    
    print("Verbunden mit dem WLAN.")
    return wlan

# Funktion zum Auslesen von DHT11-Daten
def read_dht():
    sensor_data = dht.DHT11(Pin(dht_pin))
    sensor_data.measure()
    temp = sensor_data.temperature()
    hum = sensor_data.humidity()
    print('Temperatur - ', temp, 'Luftfeuchtigkeit', hum)
    return temp, hum

# Verbinde mit dem WLAN
wifi = connect_to_wifi()

# Erstelle einen MQTT-Client
client = MQTTClient("esp32", broker_ip)

# Verbinde mit dem MQTT-Broker
client.connect()

# Loop für das Senden von DHT11-Daten über MQTT
try:
    while True:
        temperature, humidity = read_dht()
        
        # Sende Daten über MQTT
        # Das Gradsymbol kann Probleme beim Publishing verursachen
        payload = f"Temperatur: {temperature} Grad Celsius, Luftfeuchtigkeit: {humidity} %"
        client.publish(topic, payload)
        
        time.sleep(10)  # Wartezeit zwischen den Messungen
except KeyboardInterrupt:
    print("Programm beendet.")
finally:
    # Trenne die Verbindung zum MQTT-Broker
    client.disconnect()
    # Trenne die WLAN-Verbindung
    wifi.disconnect()
    wifi.active(False)
```