# getting started esp32

### Thonny - ESP Plugin installieren

tools

- manage plugins 
  - search esptool 
    - install

Video: <https://youtu.be/1efsWgK_QaU?si=uDVjiGhvB9s6RIFH&t=212>

### Thonny - Micropython flashen

run
select interpreter
micropython esp32

### Micropython aus ESP32 Getting started

Auf der esp32 Konsole....

```micropython:

>>> print("hello world")
```

```micropython:

>>> help()
```

GPIO Zugriff über das machine Modul

```micropython:

>>> import machine
>>> LED = machine.Pin(2,machine.Pin.OUT)
>>> LED.value(1)

>>> LED.value(0)
>>> LED.value(1)
```

erstes Skript auf dem ESP32 als main.py

```python:

import machine
import time

LED = machine.Pin(2,machine.Pin.OUT)

while True:
	LED.value(1)
	time.sleep(1)
	LED.value(0)
	time.sleep(1)
```

# DHT11 Temperatur und Luftfeuchtigkeitsmessung esp32

### Verkabelung:

3v  
GRND  
P14 >> Data

### Step01 - Sensor mit Micropython in der shell / Thonny testen:

```Micropython:
>>> from machine import Pin
>>> import dht
>>> 
>>> sensor_data = dht.DHT11(Pin(14))

>>> sensor_data.measure()

>>> print(sensor_data.temperature())
```

### Step02- temp und hum in Endlosschleife auslesen und ausgeben

```python:
from machine import Pin
import dht
import time
 
sensor_data = dht.DHT11(Pin(14))
 
def call_dht():
    sensor_data.measure()
    temp = sensor_data.temperature()
    hum = sensor_data.humidity()
    print('Temperatur - ',temp,'Luftfeuchtigkeit',hum)
    

while True:
    call_dht()
    time.sleep(1)
```

### Step03- Ausgelesene Daten via MQTT an Broker senden

```Python:

import network
from umqtt.simple import MQTTClient
from machine import Pin
import dht
import time

# Das Skript liest die Messdaten eines Sensors (DHT11) aus und übermittelt die Temp daten vis MQTT unter einem Tag

# Pin-Belegungen für DHT11-Sensor
dht_pin = 14

# Der esp32 sendet DHT11-Daten via mqtt
# Setze die Werte entsprechend deiner WLAN Konfiguration
ssid = "SSID"
password = "password"
broker_ip = "IP"
topic = "/cdp/room1/temp/data"

# Funktion zur Verbindung mit dem WLAN
def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    
    if not wlan.isconnected():
        print(f"Verbindung zum WLAN wird hergestellt... SSID: {ssid}")
        wlan.connect(ssid, password)
        
        while not wlan.isconnected():
            time.sleep(1)
    
    print("Verbunden mit dem WLAN.")
    return wlan

# Funktion zum Auslesen von DHT11-Daten
def read_dht():
    sensor_data = dht.DHT11(Pin(dht_pin))
    sensor_data.measure()
    temp = sensor_data.temperature()
    hum = sensor_data.humidity()
    print('Temperatur - ', temp, 'Luftfeuchtigkeit', hum)
    return temp, hum

# Verbinde mit dem WLAN
wifi = connect_to_wifi()

# Erstelle einen MQTT-Client
client = MQTTClient("esp32", broker_ip)

# Verbinde mit dem MQTT-Broker
client.connect()

# Loop für das Senden von DHT11-Daten über MQTT
try:
    while True:
        temperature, humidity = read_dht()
        
        # Sende Daten über MQTT
        payload = f"Temperatur: {temperature} °C, Luftfeuchtigkeit: {humidity} %"
        client.publish(topic, payload)
        
        time.sleep(10)  # Wartezeit zwischen den Messungen
except KeyboardInterrupt:
    print("Programm beendet.")
finally:
    # Trenne die Verbindung zum MQTT-Broker
    client.disconnect()
    # Trenne die WLAN-Verbindung
    wifi.disconnect()
    wifi.active(False)
```