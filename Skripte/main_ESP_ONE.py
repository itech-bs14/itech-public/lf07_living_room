import network
from umqtt.simple import MQTTClient
from machine import Pin
import dht
import time

# Pin-Belegungen für DHT11-Sensor
dht_pin = 14


# Der esp32 sendet DHT11-Daten via mqtt
# Setze die Werte entsprechend deiner WLAN Konfiguration

#ITECH
ssid = "SSID"
password = "password"
broker_ip = "IP"

# topic der gesendeten msg (Tempdaten)
topic1 = "cdp/room1/temp/data"


# Funktion zur Verbindung mit dem WLAN
def connect_to_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    
    if not wlan.isconnected():
        print(f"Verbindung zum WLAN wird hergestellt... SSID: {ssid}")
        wlan.connect(ssid, password)
        
        while not wlan.isconnected():
            time.sleep(1)
    
    print("Verbunden mit dem WLAN.")
    return wlan

# Funktion zum Auslesen von DHT11-Daten
def read_dht(sensor):
    sensor.measure()
    temp = sensor.temperature()
    hum = sensor.humidity()
    return temp, hum


# Verbinde mit dem WLAN
wifi = connect_to_wifi()
# Erstelle einen MQTT-Client
client = MQTTClient("esp_one", broker_ip) #name des clients darf nur einmal im system vorkommen!


# Verbinde mit dem MQTT-Broker
client.connect()


# Initialisiere den DHT11-Sensor
sensor = dht.DHT11(Pin(dht_pin))
time.sleep(2)  # Wartezeit zur Stabilisierung des Sensors

# Loop für das Senden von DHT11-Daten über MQTT (topic1) und Empfangen von Nachrichten (topic2)
try:
    while True:
        try:
            temperature, humidity = read_dht(sensor)
            
            # Sende Daten über MQTT als float unter topic1
            payload_topic1 = str(float(temperature))  # Konvertiere zu float und dann zu String
            client.publish(topic1, payload_topic1)
            
            
        except OSError as e:
            print(f"Fehler beim Auslesen des DHT11-Sensors: {e}")
        
        time.sleep(1)  # Wartezeit zwischen den Messungen
        
except KeyboardInterrupt:
    print("Programm beendet.")
finally:
    # Trenne die Verbindung zum MQTT-Broker
    client.disconnect()
    # Trenne die WLAN-Verbindung
    wifi.disconnect()
    wifi.active(False)
