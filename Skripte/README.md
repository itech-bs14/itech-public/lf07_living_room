# iot_basics

## Skripte IoT_basics_für das erweiterte Szenario LF07

Die Skripte für die ESP32 Microcontroller sind immer main_NamedesControlers.py benannt (siehe [System Aufbau](../IoT_basics_mqtt_Aufbau.excalidraw.png?fileId=160956)). Die Micropython Skripte müssen via einer IDE (z.B. [Thonny]()) auf den ESP32 geladen werden und dort als main.py gespeichert werden!

Hierzu muss zuvor Micropython auf den ESP32 geflasht werden ([Anleitung](https://www.youtube.com/watch?v=1efsWgK_QaU))

Eine Version MIcropython für die hier benutzen ESP32 ist im Verzeichnis (ESP32_GENERIC_2020105-v1.22.1.bin)

## Skripte für die ESP32

## ESP_ONE

##### **main_ESP_ONE.py**

Skript liest einen Sensor aus (DHT11) und sendet die Temperaturdaten unter einem tag () via MQTT an den Broker (publish).

## ESP_TWO

##### **main_ESP_TWO.py**

Skript erhält vom Broker Daten eines bestimmten Tags (subscribe) und je nach Wert aktiviert er einen Aktor (LED / Ventilator).

## ESP_THREE

##### **main_ESP_THREE.py**

Skript verbindet die beiden Skripte von ESP_ONE und ESP_TWO. Es werden Daten die durch einen Sensor erfasst werden via MQTT unter einem tag an den MQTT Broker gesendet (publish) sowie gleichzeitig wird ein zweiter Tag vom Broker abonniert (subscribe) und je nach Wert ein Aktor aktiviert.

## Skripte für den RasPi (Pi_ONE)

Aufgaben:

Auslesen der gemessenen gemessenen Daten (MQTT Tag: tags cdp/room1/temp/data) in eine Datenbank.

Auslesen der gemessnen / gespeicherten Datenreihe (10 Datensätze) und Berechnung und speicherung des arithmetischen Mittels.

Versenden des arithmetischen Mittels via MQTT ((MQTT Tag: tags cdp/room1/temp/display)) an die entsprechenden Aktoren

die Skripte

1. database.py:
   - Enthält die Funktionen zur Initialisierung der Datenbank, zum Einfügen von Temperaturen und zur Berechnung des arithmetischen Mittels der letzten zehn Einträge.
2. mqtt_handler.py:
   - Enthält die Callback-Funktion on_message für empfangene MQTT-Nachrichten und die Funktion setup_mqtt_client zur Konfiguration des MQTT-Clients.
3. main.py:
   - Initialisiert die Datenbank, konfiguriert den MQTT-Client und startet die Endlosschleife, um Nachrichten zu empfangen und zu verarbeiten.

Diese Struktur trennt die Verantwortlichkeiten klar und macht den Code modularer und einfacher zu warten.

...

## weitere Skripte

##### **main_ESP_simpleDHT11.py**

Ein Skript reduziert auf das Ansteuern bzw. auslesen eines Sensors (DHT11) zur Erforschung und zum Einstieg. Auf dem ESP32 als main.py speichern und ausführen!

<<Link zum Schaltplan>>

##### **main_ESP_get_ip.py**

Ein Skript zum auslesen der IP eines ESP32 MC\`s. Als main.py auf dem ESP ausführen!  
Wichtig um den MC ins SchulWLAN zu bekommen.