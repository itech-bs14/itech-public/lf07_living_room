# Pi_ONE

## Vorbereitung

### Installation


```bash
# sudo apt install ...
# python3 # Für den Pi
# thonny # IDE für Python und MicroPython. Ist geeignet zum flashen von MicroPython auf ESP32
# vim
# apt-add-repository ppa:mosquitto-dev/mosquitto-ppa # Optional
# mosquitto # Server bzw. Broker
# mosquitto-clients 
# mariadb-server # MYSQL Datenbank 
# sqlite3 # Für die Dateibasierte Sicherung der Daten


# Optional, wenn man mit einer zentralen DB arbeiten möchte
# apache2 # Webserver
# phpmyadmin # Oberfläche zum Umgang mit der DB

# Pythonbibliotheken
# mysql-connector-python # Pythonbibliothek für MySQL
```

### Konfiguration

- Mac Adresse --> Teacher WLAN

## Py Skripte auf Pi_ONE:

---

Auslesen der gemessenen gemessenen Daten (MQTT Tag: tags cdp/room1/temp/data) in eine Datenbank.

Auslesen der gemessenen / gespeicherten Datenreihe (10 Datensätze) und Berechnung und Speicherung des arithmetischen Mittels.

Versenden des arithmetischen Mittels via MQTT ((MQTT Tag: tags cdp/room1/temp/display)) an die entsprechenden Aktoren

### Skript 1: `database.py`

```python
import sqlite3

# Datenbankname. Diese Datei wird in dem Ordner angelegt, wo das Skript liegt.
db_name = 'temperature_data.db'

# erzeugt eine Tabelle mit drei Spalten für die Sicherung der Daten.
def initialize_database():
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute('''
        CREATE TABLE IF NOT EXISTS temperature_data (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
            temperature REAL
        )
    ''')
    conn.commit()
    conn.close()

# Fügt die Temperaturwerte in die Tabelle ein. Id und Timestamp werden automatisch gesezt.
def insert_temperature(temperature):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute('''
        INSERT INTO temperature_data (temperature)
        VALUES (?)
    ''', (temperature,))
    conn.commit()
    conn.close()
    print("Daten in DB eingetragen.")

# Berechnet die Durchschnittstemperatur. Die Funktion fragt die letzten zehn Werte aus der Datenbank ab und verarbeitet diese.
def calculate_average_temperature():
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    c.execute('''
        SELECT temperature FROM temperature_data
        ORDER BY timestamp DESC
        LIMIT 10
    ''')
    rows = c.fetchall()
    conn.close()

    if rows:
        temperatures = [row[0] for row in rows]
        average_temperature = sum(temperatures) / len(temperatures)
    else:
        average_temperature = None

    return average_temperature
```

### Skript 2: `mqtt_handler.py`

```python
import paho.mqtt.client as mqtt
from database import insert_temperature, calculate_average_temperature

# MQTT-Parameter
broker_ip = "10.14.213.223"
topic_data = "cdp/room1/temp/data"
topic_display = "cdp/room1/temp/display"


# Callback-Funktion für empfangene MQTT-Nachrichten
def on_message(client, userdata, msg):
    payload = msg.payload.decode()
    temperature = float(payload)

    # Temperatur in die Datenbank einfügen
    insert_temperature(temperature)

    # Arithmetisches Mittel berechnen
    average_temperature = calculate_average_temperature()

    # Durchschnittstemperatur publizieren
    if average_temperature is not None:
        client.publish(topic_display, str(average_temperature))
        print(f"Published average temperature: {average_temperature}")


def setup_mqtt_client():
    client = mqtt.Client()

    # Setze die neuen Callback-Funktionen
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(broker_ip)
    return client


# Callback-Funktion für erfolgreiche Verbindungen
def on_connect(client, userdata, flags, rc):
    print("Verbunden mit dem Broker")
    client.subscribe(topic_data)
```

### Skript 3: `main.py`

```python
import time
from database import initialize_database
from mqtt_handler import setup_mqtt_client


def main():
    # Datenbank initialisieren
    initialize_database()

    # MQTT-Client konfigurieren
    client = setup_mqtt_client()

    # Endlosschleife, um Nachrichten zu empfangen und zu verarbeiten
    client.loop_start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print("Programm beendet.")
    finally:
        client.loop_stop()
        client.disconnect()


if __name__ == "__main__":
    main()
```

### Erklärung:

1. `database.py`:
   - Enthält die Funktionen zur Initialisierung der Datenbank, zum Einfügen von Temperaturen und zur Berechnung des arithmetischen Mittels der letzten zehn Einträge.
2. `mqtt_handler.py`:
   - Enthält die Callback-Funktion `on_message` für empfangene MQTT-Nachrichten und die Funktion `setup_mqtt_client` zur Konfiguration des MQTT-Clients.
3. `main.py`:
   - Initialisiert die Datenbank, konfiguriert den MQTT-Client und startet die Endlosschleife, um Nachrichten zu empfangen und zu verarbeiten.

Diese Struktur trennt die Verantwortlichkeiten klar und macht den Code modularer und einfacher zu warten.