# ESP_TWO - Daten via MQTT erhalten und damit einen Aktor ansteuern

Der Esp32 MC ESP_TWO

Die Skripte auf dem ESP32 sind in Micropython geschrieben, da die Schüler in den vorherigen LF bereits erste Erfahrungen mit Python gemacht haben und es hier keine Umstellung gibt.

**MAC Adresse ESP_ONE:**

## Vorbereitung

- Installieren von [Thonny](https://thonny.org/) als IDE und zum flashen von Micropython auf den ESP32
  - Download [Micropython](https://micropython.org/download/)
  - Flashen von Micropython auf den ESP32.  
    <https://youtu.be/1efsWgK_QaU?si=gooJbZJtA0LSO-N9>
- Mac Adresse auslesen um den ESP32 im Schul WLAN anmelden

  ```Python
  import network
  
  # Initialize the network interface
  wlan = network.WLAN(network.STA_IF)
  # Activate the WLAN Interface
  wlan.active(True)
  
  # Check if the interface is active (connected)
  if wlan.active():
      # Get the MAC address
      mac_address = wlan.config("mac")
      print(mac_address)
      print("Device MAC Address:", ":".join(["{:02X}".format(byte) for byte in mac_address]))
  else:
      print("Wi-Fi is not active.")
  ```

Nach dem flashen von Micropython sind zwei .py Skripte auf dem ESP32 (Boot.py und main.py). Es wird die main.py jeweils überschrieben. Das Skript s.o. als main.py auf dem ESP32 speichern und ausführen. Mac Adresse erscheint im Terminal.

## Das Skript auf dem ESP_TWO

### Verkabelung:

3v GRND P14 >> Data

\>> Schaltplan folgt

### Step01 - Sensor mit Micropython in der shell / Thonny

### Step02- temp und hum in Endlosschleife auslesen und au

### Step03- Ausgelesene Daten via MQTT an Broker senden (main_ESP_ONE.py als main.py ausführen)

```Python:
```