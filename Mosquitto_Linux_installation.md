## Mosquitto installieren

Mosquitto kann mit dem Paketmanager `apt` heruntergeladen werden. Dazu öffnet man ein Terminal:

:~\> `sudo apt install mosquitto`

Neben dem Broker sollten auch die Clients installiert werden.

:~\> `sudo apt install mosquitto-clients`

Nach dem Installieren kann Mosquitto mit der -v Option (verbose) gestartet werden.

:~\> `mosquitto` `-v`

1672050687: mosquitto version 2.0.10 starting  
1672050687: Using default config.  
1672050687: Starting in local only mode. Connections will only be possible from clients running on this machine.  
1672050687: Create a configuration file which defines a listener to allow remote access.  
1672050687: For more details see [https://mosquitto.org/documentation/authentication-methods/](https://mosquitto.org/documentation/authentication-methods/%EF%BF%BC1672050687)  
[1672050687](https://mosquitto.org/documentation/authentication-methods/%EF%BF%BC1672050687): Opening ipv4 listen socket on port 1883.  
1672050687: Opening ipv6 listen socket on port 1883.  
1672050687: mosquitto version 2.0.10 running  
1672050692: mosquitto version 2.0.10 terminating

So kann man überprüfen, welche Version von MQTT installiert ist und auf welchen Port Mosquitto hört. Normalerweise sollte dies der Port 1883 sein.

[Link to the video in English language](https://youtu.be/jN1gVyM8k6Q)

## Mosquitto testen

Ist Mosquitto und die Mosquitto-Clients auf dem System installiert, kann ein einfacher Funktionstest erfolgen.

### Mosquitto Subscriber-Client testen

In einem Terminal wird ein Mosquitto Subscriber-Client gestartet.

:~\> `mosquitto_sub` `-h localhost` `-t test1`

`mosquitto_sub` `-h localhost` `-t test1`

|    |           |                                                                                            |
|----|-----------|--------------------------------------------------------------------------------------------|
| `-h` | `localhost` | IP des Brokers oder localhost, wenn der Broker auf dem selben Rechner wie der Client läuft |
| `-t` | `test1`     | Bezeichner des Topics, das der Subscriber-Client abonnieren soll.                          |

Der Subscriber-Client kann mit der Tastenkombination `Strg + c` beendet werden.

### Mosquitto Publisher-Client testen

In einem anderen Terminal wird ein Mosquitto Publisher-Client gestartet.

:~\> `mosquitto_pub` `-h localhost` `-t test1` `-m Hallo!`

`mosquitto_pub` `-h localhost` `-t test1` `-m Hallo!`

|    |           |                                                                                                                                                                  |
|----|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `-h` | `localhost` | IP des Brokers oder localhost, wenn der Broker auf dem selben Rechner wie der Client läuft                                                                       |
| `-t` | `test1`     | Bezeichner des Topics, das der Subscriber-Client abonnieren soll.                                                                                                |
| `-m` | `Hallo!`    | Die Nachricht, die veröffentlicht werden soll.  <br />Nachrichten, die mehrere mit `Space` getrennte Wörter enthalten, müssen in Anführungszeichen gesetzt werden. |

Die Nachricht sollte, sobald sie vom Publisher veröffentlicht wird, im Terminal des Subscribers auftauchen.

[Link to the video in English language](https://youtu.be/jN1gVyM8k6Q)

Für die Kommunikation via MQTT im LAN muss der Mosquitto Broker konfiguriert werden:

## Konfiguration Mosquitto im LAN

Mosquitto muss für die Kommunikation im LAN konfiguriert werden. Dazu muss die Konfigurationsdatei `mosquitto.conf` angepasst werden, die sich bei der Installation auf dem Raspberry pi im Verzeichnis `/etc/mosquitto` befindet.  
Die Datei kann nur mit Root-Rechten geschrieben werden. Dies muss man beachten, wenn man die Datei z.B. mit Nano editieren möchte.

Am Ende der Datei müssen zwei Einträge ergänzt werden:

`listener 1883`

`allow_anonymous true`

Der erste Eintrag bewirkt, dass Mosquitto auf den Port 1883 hört.

Der zweite Eintrag bewirkt, dass Mosquitto auch nicht registrierte (anonyme) Clients zulässt.

Nach dem Speichern der Konfigurationsdatei muss Mosquitto neu gestartet werden. Das Kommando für den Neustart kann je nach Linux-System variieren. Bei der Installation auf einem Raspberry pi kann Mosquitto mit folgendem Befehl neu gestartet werden:

:~\> `sudo systemctl restart mosquitto`

## Test der Kommunikation via MQTT im LAN

### Ermitteln der IP Adresse des Brokers

Die IP-Adresse des Rechners (in diesem Beispiel der Raspberry pi) auf dem der Mosquitto Broker läuft muss ermittelt werden. Dazu gibt man folgenden Befehl in einem Terminal ein:

:~\> `ip addr show`

Aus der Bildschirmausgabe kann die IP-Adresse abgelesen werden.  
Die IP-Adresse wird für jeden Client benötigt, der über das LAN mit dem Broker kommunizieren muss.

### Test der Kommunikation

In einem Terminal auf dem Raspberry pi wird ein Mosquitto Subscriber-Client gestartet.

:~\> `mosquitto_sub` `-h localhost` `-t test1`

Auf einem anderen Rechner im LAN wird in einem Terminal wird ein Mosquitto Publisher-Client gestartet.

:~\> `mosquitto_pub` `-h Broker-IP` `-t test1` `-m Hallo!`

`mosquitto_pub` `-h localhost` `-t test1` `-m Hallo!`

|  |  |  |
|--|--|--|
